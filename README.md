# scholarships (Teste frontend na QueroEducação)

> Projeto em Vue para listar bolsas de estudos e ser possível adicioná-las em uma lista de favoritos.

Link: https://safire-teste-quero.netlify.com/

## Minhas considerações
Quando recebi o teste prático fiquei em dúvida em qual stack utilizar, se seria em AngularJS ou em Ember que são as que eu já tenho experiência.

Decidi fazê-lo em VueJs com a ideia de já utilizar algo que já faz parte da stack da Quero Educação.

Nunca tinha feito nada em Vue e pra mim foi um desafio bem bacana onde pude praticar minhas habilidades e aprender um framework novo. Pude notar algumas similaridades com Ember e AngularJS, mas ainda tive alguns desafios uma vez que nuca havia tido um contato mais direto com o framework.

Acredito que há lugares onde deva existir melhores formas de aplicar o Vue, que eu talvez ainda não conheça, mas apesar disso, achei bem produtivo e fácil de se trabalhar.

No meio do desenvolvimento, a API utilizada no teste (testapi.io) atingiu o limite de requests por mês, e tive que trabalhar com dados fake não conseguindo garantir a integridade dos dados e do layout onde esses dados são dinâmicos.

Se esse limite ainda estiver ativo, ao executar a aplicação, não listará nenhuma bolsa de estudo. Por esse motivo, irei deixar a chamada para a API com o código comentado e irei utilizar uma lista de dados mockados para que seja possível ver as funcionalidades da aplicação.

Caso queiram utilizar a chamada real dos dados pela API deve-se:
- Remover os objetos mockados de dentro do array de `scholarships`, tornando-o um array vazio, no arquivo `src/store/store.js`;
- Descomentar a requisição da API em `getScholarships()` no arquivo `src/components/Main.vue`;

## Para rodar o projeto

``` bash
# install dependencies
yarn

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# run unit tests
npm run test
```
