// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core';
import {
  faInfo,
  faUser,
  faChevronDown,
  faChevronUp,
  faChevronLeft,
  faPlus,
  faStarHalfAlt,
  faCircle,
  faTimes,
  faStar as faStarRegular } from '@fortawesome/free-solid-svg-icons';
import { faComments, faEnvelope, faHeart, faStar } from '@fortawesome/free-regular-svg-icons';
import { faWhatsapp } from '@fortawesome/free-brands-svg-icons';
import axios from 'axios';
import Vue from 'vue';
import App from './App';
import store from './store/store';

library.add(
  faInfo,
  faUser,
  faWhatsapp,
  faChevronDown,
  faChevronUp,
  faChevronLeft,
  faPlus,
  faStar,
  faStarRegular,
  faStarHalfAlt,
  faCircle,
  faComments,
  faEnvelope,
  faHeart,
  faTimes,
);

Vue.prototype.$http = axios;

Vue.config.productionTip = false;

Vue.component('font-awesome-icon', FontAwesomeIcon);

Vue.filter('currency', valor => `R$ ${parseFloat(valor).toFixed(2)}`.replace('.', ','));

/* eslint-disable no-new */
new Vue({
  store,
  el: '#app',
  components: { App },
  template: '<App/>',
});
