/* eslint-disable quotes */
import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    scholarships: [
      {
        full_price: 600.98,
        price_with_discount: 300.5,
        discount_percentage: 5.0,
        start_date: "08/09/2020",
        enrollment_semester: "2020.1",
        enabled: true,
        course: {
          name: "Administração",
          kind: "presencial",
          level: "Bacharel",
          shift: "Noite",
        },
        university: {
          name: "Estacio",
          score: "4.5",
          logo_url:
            "https://qb-assets.querobolsa.com.br/logos/colorido/large/19/logo_1560291750.png",
        },
        campus: {
          name: "Estacio F",
          city: "Fortaleza",
        },
      },
      {
        full_price: 900.0,
        price_with_discount: 310.5,
        discount_percentage: 50.0,
        start_date: "08/09/2020",
        enrollment_semester: "2020.1",
        enabled: true,
        course: {
          name: "Inteligência Artificial",
          kind: "presencial",
          level: "Bacharel",
          shift: "Noite",
        },
        university: {
          name: "Estacio",
          score: "3.7",
          logo_url: "https://qb-assets.querobolsa.com.br/logos/colorido/large/19/logo_1560291750.png",
        },
        campus: {
          name: "Estacio F",
          city: "São Paulo",
        },
      },
      {
        full_price: 540.98,
        price_with_discount: 210.5,
        discount_percentage: 5.0,
        start_date: "08/09/2020",
        enrollment_semester: "2020.1",
        enabled: true,
        course: {
          name: "Inteligência Artificial",
          kind: "presencial",
          level: "Bacharel",
          shift: "Noite",
        },
        university: {
          name: "Estacio",
          score: "2.7",
          logo_url:
            "https://qb-assets.querobolsa.com.br/logos/colorido/large/27/logo_1489432810.png",
        },
        campus: {
          name: "Estacio F",
          city: "Diadema",
        },
      },
      {
        full_price: 500.98,
        price_with_discount: 300.5,
        discount_percentage: 5.0,
        start_date: "08/09/2020",
        enrollment_semester: "2020.1",
        enabled: true,
        course: {
          name: "Inteligência Artificial",
          kind: "presencial",
          level: "Bacharel",
          shift: "Noite",
        },
        university: {
          name: "Estacio",
          score: "4.8",
          logo_url: "https://qb-assets.querobolsa.com.br/logos/colorido/large/19/logo_1560291750.png",
        },
        campus: {
          name: "Estacio F",
          city: "Guarulhos",
        },
      },
      {
        full_price: 500.98,
        price_with_discount: 300.5,
        discount_percentage: 5.0,
        start_date: "08/09/2020",
        enrollment_semester: "2020.1",
        enabled: true,
        course: {
          name: "Inteligência Artificial",
          kind: "distancia",
          level: "Bacharel",
          shift: "Noite",
        },
        university: {
          name: "Estacio",
          score: "3.7",
          logo_url:
            "https://qb-assets.querobolsa.com.br/logos/colorido/large/27/logo_1489432810.png",
        },
        campus: {
          name: "Estacio F",
          city: "São Caetano do Sul",
        },
      },
      {
        full_price: 239.0,
        price_with_discount: 155.35,
        discount_percentage: 35.0,
        start_date: "08/09/2020",
        enrollment_semester: "2020.1",
        enabled: true,
        course: {
          name: "MBA em Segurança de Informação",
          kind: "presencial",
          level: "Bacharel",
          shift: "Manhã",
        },
        university: {
          name: "Estacio",
          score: "3.7",
          logo_url:
            "https://qb-assets.querobolsa.com.br/logos/colorido/large/27/logo_1489432810.png",
        },
        campus: {
          name: "Estacio F",
          city: "São Caetano do Sul",
        },
      },
    ],
    favoriteScholarships: localStorage.getItem('favoriteCourses') ? localStorage.getItem('favoriteCourses').split(',') : [],
  },
  mutations: {
    addFavoriteScholarship(state, courseId) {
      state.favoriteScholarships.push(courseId);
    },
    setFavoriteScholarships(state, courses) {
      state.favoriteScholarships = courses;
    },
  },
  actions: {
    removeFavoriteCourse(context, courseId) {
      const newCourses = context.state.favoriteScholarships.filter(id => id !== `${courseId}`);

      context.commit('setFavoriteScholarships', newCourses);
    },
  },
});
